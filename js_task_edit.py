#!/usr/bin/python3

from argparse import ArgumentParser
from urllib.request import urlopen
from shutil import rmtree
from json import loads as toJSON
import re
import string
import os
import subprocess

from npm2deb import Npm2Deb as _Npm2Deb
from npm2deb import Mapper as _Mapper


DEFAULT_OUTPUT = 'js_task_page'
DEFAULT_PACKAGE_FILE = 'package.json'
TMP = '/tmp/jswiki'
modules = {}
SRC, _ = os.path.split(__file__)

map_npm_debian = {
    'uglify-js': 'node-uglify'
  , 'coffee-script': 'coffeescript'
}

def get_output(cmd, force=False):
    try:
        info = subprocess.check_output(cmd,shell=True)
    except subprocess.CalledProcessError:
        if force:
            return ''
        else:
            return ''
            exit(1)
    return info.decode('utf-8')


def get_table_line(tree, npm, debian):
    debianstr=''
    npmstr=''
    if debian == None:
        debianstr = ''
    elif debian['type'] == 'packaged':
        debversion = debian['version']
        versionok = ''
        if npm['version'] != None:
                debupversion  = re.sub('[-]\d+$','',debversion)
                ret = subprocess.call(['dpkg','--compare-versions',npm['version'],'le',debupversion])
                if ret == 0:
                        versionok = ' {OK} '
                elif ret == 1:
                        versionok = ' /!\\ '
                else:
                        versionok = ''
        debianstr = '%s (%s%s)' % (debian['name'], debian['version'],versionok)
    else:
        bugtype = debian['type']
        bug = debian['bug']
        name = debian['name']
        debianstr = '%s [[http://bugs.debian.org/%s|%s  #%s"]]' % (bugtype, bug, name, bug)
    if npm['version'] == None:
        npmstr = npm['name']
    else:
        npmstr = '%s (%s)' % (npm['name'],npm['version'])

    return '||<style="font-family: courier, monospace; \
white-space: pre-wrap; word-wrap: break-word;"> \
%s || %s || %s || ||' % (tree, npmstr, debianstr)

def get_npm_version(mondule_name):
    output = get_output('npm view %s version 2>/dev/null' % mondule_name)
    if output is not None and len(output) > 0:
        return output.strip()
    else:
        return None

def get_debian(module_name):
    print("Looking for package: %s" % module_name)
    npm2deb_instance = _Npm2Deb(module_name)
    mapper = _Mapper.get_instance()
    mapped = mapper.get_debian_package(npm2deb_instance.name)
    print("mapped",mapped)
    if mapped['name'] == None:
        print("Package not found in Debian. Looking for ITP")
        ITPret = get_output("""bash "%s/bug-check.sh" %s | head -1""" % (SRC,module_name), force=True)
        if ITPret == '' :
                return None
        else:
                fields = ITPret.split(' ')
                ret = { 'type' : fields[0].strip(), 'name' : fields[2].strip(), 'bug' : fields[1].strip() }
                return ret
    else:
        ret = { 'type' : 'packaged', 'name' : mapped['name'], 'version' : mapped['version'] }
        return ret

def make_tree_module_dependencies(module_name, base="treedir"):
    deb_dir = "%s/%s" % (base, module_name)
    print("Getting info: %s" % (deb_dir.replace('treedir', '')))
    get_output("mkdir -p %s" % deb_dir)
    if module_name in modules:
        return
    modules[module_name] = get_npm_version(module_name)
    try:
        dependencies = toJSON(\
            get_output("npm view %s dependencies --json 2>/dev/null" % module_name))
        for dep in dependencies:
            make_tree_module_dependencies(dep, deb_dir)
    except ValueError:
        return


def get_dependencies(module_name, deps=""):
    # save current dir
    savedpath = os.getcwd()

    # create tmp dir
    if os.path.isdir(TMP):
        rmtree(TMP)
    os.mkdir(TMP)
    os.chdir(TMP)
    # create tree
    if isinstance(deps, dict):
        for module in deps:
            make_tree_module_dependencies(module, 'treedir/%s' % module_name)
    else:
        make_tree_module_dependencies(module_name)
    # get tree
    os.chdir('treedir')
    tree = get_output("tree --noreport %s" % module_name).split('\n')
    print("Creating table dependencies")
    table = []
    for module in tree:
        try:
            npmname = module.strip().split()[-1]
            npmvers = modules[npmname] if npmname in modules else None
            npm = {'name' : npmname, 'version' : npmvers }
            deb = get_debian(npmname)
            table.append(get_table_line(module, npm, deb))
        except:
            None

    os.chdir(savedpath)
    rmtree(TMP)

    if len(table) > 0:
        return '\n'.join(table)
    else:
        return "'''None'''"

def get_builddeps(module_name, deps=""):
    dev = []
    if not isinstance(deps, dict):
        json = toJSON(get_output('npm view %s --json 2>/dev/null' % module_name))
        if 'devDependencies' in json:
            deps = json['devDependencies']
    if isinstance(deps, dict):
        for dep in deps:
            npmvers = get_npm_version(dep)
            npm = {'name' : dep , 'version' : npmvers }
            debian = get_debian(dep)
            dev.append(get_table_line(dep, npm, debian))
        return "\n".join(dev)
    return "||'''None'''||"


def main():

    if not os.path.isfile('/usr/bin/tree'):
        print("You must install tree")
        exit(1)

    if not os.path.isfile('/usr/bin/npm'):
        print("You must install npm")
        exit(1)

    usage = "%(prog)s [options] module_name"
    parser = ArgumentParser(prog='js_task_create', usage=usage)
    parser.add_argument('-o', '--output', default=DEFAULT_OUTPUT,
                        help="output file name [default: %s]" % DEFAULT_OUTPUT)
    parser.add_argument('-f', '--file', default=False,
                        action="store_true",
                        help="read from package description file [default: %s]"\
                        % DEFAULT_PACKAGE_FILE)
    parser.add_argument('module_name', nargs='?',
                        default=DEFAULT_PACKAGE_FILE, help='the nodejs module or package description file')

    args = parser.parse_args()

    module_name = "package"
    deps = ""
    build_deps = ""

    if args.file:
        if os.path.isfile(args.module_name):
            with open(args.module_name, 'r') as pkg:
                json = toJSON(pkg.read())
                if "name"in json:
                    module_name = json["name"]

                if "dependencies" in json:
                    deps = get_dependencies(module_name, json["dependencies"])

                if "devDependencies" in json:
                    build_deps = get_builddeps(module_name, json["devDependencies"])
        else:
            print("File %s does not exists" % args.module_name)
            exit(1)

    else :
        module_name = args.module_name
        deps = get_dependencies(module_name)
        build_deps = get_builddeps(module_name)

    template_url = "https://wiki.debian.org/Javascript/Nodejs/Tasks/Template"
    template = urlopen('%s?action=raw' % template_url).read().decode('utf-8')
    template = template.replace("''' DO NOT EDIT THIS PAGE !!!'''", '')
    template = template.replace ("name", module_name)
    template = template.replace ("dependencies", deps)
    page = template.replace ("builddep", build_deps)

    output_file = args.output

    with open(output_file, 'w') as wikipage:
        wikipage.write(page)
        print("Wiki page saved in %s file." % output_file)


if __name__ == '__main__':
    main()
